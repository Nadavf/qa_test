#!/usr/bin/python
import sys 
import os
import requests
import time

sys.path.append('/root/catkin_ws/src/vision/src/common_tools')
sys.path.append('/root/catkin_ws/src/vision/src')
from common_tools import publishers


DRONE_GW_URL = "http://localhost:1001"
DRONE_GW_HEADER = {"1": "Content-Type: application/json", }

ROS_ROUTE = DRONE_GW_URL + "/ros"
RECORDER_ROUTE = DRONE_GW_URL + "/recorder"
TOOL_ROUTE = DRONE_GW_URL + "/tool"
CAMERA_ROUTE = DRONE_GW_URL + "/camera"
PLAYER_ROUTE = DRONE_GW_URL + "/player"
USER_INPUT_ROUTE = DRONE_GW_URL + "/userInput"

ARM_PAYLOAD = "{\"cmd\": \"arm\"}"
TAKEOFF_PAYLOAD = "{\"cmd\": \"takeoff\", \"params\": {\"height\": 1.2}}"
LAND_PAYLOAD = "{\"cmd\": \"setMode\", \"params\": {\"mode\": \"LAND\"}}"
GUIDED_PAYLOAD = "{\"cmd\": \"setMode\", \"params\": {\"mode\": \"OFFBOARD\"}}"


def tele_test():
    requests.post(url=ROS_ROUTE, data=ARM_PAYLOAD, headers=DRONE_GW_HEADER)
    time.sleep(5)
    requests.post(url=ROS_ROUTE, data=TAKEOFF_PAYLOAD, headers=DRONE_GW_HEADER)
    time.sleep(8)
    requests.post(url=ROS_ROUTE, data=GUIDED_PAYLOAD, headers=DRONE_GW_HEADER)

def end_test():
    NavigationPublishers._pix4_Navigator([0.0, 0.0, 0.0], 0, 'body')
    time.sleep(10)
    requests.post(url=ROS_ROUTE, data=LAND_PAYLOAD, headers=DRONE_GW_HEADER)
    
#     VisNavigate.Navigate2Target([0.0, 0.0, 0], 0, 'body')

